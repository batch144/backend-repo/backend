
const User = require("./../models/User");
const Course = require("./../models/Course");

const bcrypt = require("bcrypt");
const auth = require("./../auth");

module.exports.checkEmail = (reqBody) => {
	const {email} = reqBody

	return User.findOne({email: email}).then( (result, error) => {

		if(result != null){
			return `Email already exists`
		} else {
			if(result == null){
				return true
			} else {
				return error
			}
		}
	})
}

//register
module.exports.register = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})
	//save()
	return newUser.save().then( (result, error) => {
		if(result){
			return true
		} else{
			return error
		}
	})
}

//retrieve all users
module.exports.getAllUsers = () => {

	return User.find().then( (result, error) => {
		if(result){
			return result
		} else {
			return error
		}
	})
}

//login
module.exports.login = (reqBody) => {
	const {email, password} = reqBody;
	return User.findOne({email: email}).then( (result, error) => {

		if(result == null){
			return false
		} else {
			//what if we found the email and is existing, but the password is incorrect

			let isPasswordCorrect = bcrypt.compareSync(password, result.password)

			if(isPasswordCorrect == true){
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

//retrieve user information
module.exports.getProfile = (data) => {

	const {id} = data

	return User.findById(id).then((result, err) => {

		if(result != null){
			result.password = "";
			return result
		} else {
			return false
		}
	})
}

//enroll user
module.exports.enroll = async (data) => {

	const {userId, courseId} = data

	//look for matching document of an user
	const userEnroll = await User.findById(userId).then((result, error) => {
		if(error){
			return error
		} else {
			//console.log(result)
			//add the courseId in the enrollments array
			result.enrollments.push({courseId: courseId})
			
			if(result.enrollments.courseId != courseId){
				//save
				return result.save().then((result, error) => {
					if(error){
						return error
					} else {
						return true
					}
				})
			} else {
				return false
			}
		}
	})

	////look for matching document of an course
	const courseEnroll = await Course.findById(courseId).then((result, error) => {
		if(error){
			return error
		} else {
			//add the userId in the enrollees array
			result.enrollees.push({userId: userId})

			if(result.enrollees.userId != userId){
				//save
				return result.save().then((result, error) => {
					if(error){
						return error
					} else {
						return true
					}
				})
			} else {
				return false
			}
		}
	})

	//to return only one value for the function enroll
	if(userEnroll && courseEnroll){
		return true
	} else {
		return false
	}
}
