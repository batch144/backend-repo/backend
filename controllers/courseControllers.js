
const Course = require("./../models/Course");

module.exports.createCourse = (reqBody) => {
	let newCourse = new Course({
		
		courseName: reqBody.courseName,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

//get all courses
module.exports.getAllcourses = () => {

	return Course.find().then((result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	})
}

//retrieve only active courses
module.exports.getActivecourses = () => {

	return Course.find({isActive: true}).then((result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	})
}

//get a spcific course using findOne()
module.exports.getSpecificCourse = (reqBody) => {

	return Course.findOne({courseName: reqBody}).then((result, error) => {
		if(result === null){
			return `Course not exist`
		} else {
			if(result){
				return result
			} else {
			return error
			}
		}
	})
}

//get specific course ysing findById
module.exports.getCourseById = (params) => {

	//console.log(params)
	return Course.findById(params).then((result, error) => {
		//console.log(params)
		if(result === null){
			return `Course not exist`
		} else {
			if(result){
				return result
			} else {
			return error
			}
		}
	})
}

//update isActive status of the course using findOneAndUpdate
	//update isActive to false
module.exports.archiveCourse = (reqBody) => {
	let courseStatus = {
		isActive: false
	}

	return Course.findOneAndUpdate({courseName: reqBody}, courseStatus).then((result, error) => {

		if(result === null){
			return `Course not exist`
		} else {
			if(result){
				return true
			} else {
			return false
			}
		}
	})
}



module.exports.unarchiveCourse = (reqBody) => {
	let courseStatus = {
		isActive: true
	}

	return Course.findOneAndUpdate({courseName: reqBody}, courseStatus).then((result, error) => {

		if(result === null){
			return `Course not exist`
		} else {
			if(result){
				return true
			} else {
			return false
			}
		}
	})
}

//find course by Id and update
module.exports.archiveCourseById = (params) => {

	return Course.findByIdAndUpdate(params, {isActive: false}).then((result, error) => {

		if(result == null){
			return `Course not exist`
		} else {
			if(result){
				return true
			} else {
			return false
			}
		}
	})
}


//find course by Id and update
module.exports.unarchiveCourseById = (params) => {

	return Course.findByIdAndUpdate(params, {isActive: true}).then((result, error) => {

		if(result == null){
			return `Course not exist`
		} else {
			if(result){
				return true
			} else {
			return false
			}
		}
	})
}

//delete course using findOneAndDelete()
module.exports.deleteCourse = (name) => {

	return Course.findOneAndDelete({courseName: name}).then((result, error) => {

		if(result == null){
			return `Course not exist`
		} else {
			if(result){
				return true
			} else {
			return error
			}
		}
	})
}

//delete course using findByIdAndDelete()
module.exports.deleteCourseById = (id) => {

	return Course.findByIdAndDelete(id).then((result, error) => {

		if(result == null){
			return `Course not exist`
		} else {
			if(result){
				return true
			} else {
			return error
			}
		}
	})
}

//update course details
module.exports.editCourse = (id, reqBody) => {

	let {courseName, description, price} = reqBody
	let updatedCourse = {
		courseName: courseName,
		description: description,
		price: price
	}

	return Course.findByIdAndUpdate(id, updatedCourse, {new: true}).then((result, error) => {

		if(error){
			return error
		} else {
			return result
		}
	})
}
